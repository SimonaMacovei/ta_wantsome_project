package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ContactUsPage {
    WebDriver driver;
    @FindBy(css = "input[placeholder='Name']")
    WebElement nameInput;

    @FindBy(css = "input[placeholder='Email']")
    WebElement emailInput;

    @FindBy(css = "input[placeholder='Subject']")
    WebElement subjectInput;

    @FindBy(css = "#message")
    WebElement yourMessageInput;

    @FindBy(css = "input[value='Submit']")
    WebElement submitButton;


    @FindBy(xpath="//h2[.='Get In Touch']")
    WebElement getInTouchText;

    @FindBy(xpath = "//input[@name='upload_file']")
     WebElement uploadFileInput;
    @FindBy(css=".status.alert.alert-success")
    WebElement alertSuccessMessage;

    @FindBy(css="a[class='btn btn-success'] span")
    WebElement homePageButton;


    public static final String URL = "https://www.automationexercise.com/contact_us";

    public ContactUsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }



    public void enterName(String name){
        nameInput.sendKeys(name);

    }
    public void enterEmail(String email){
        emailInput.sendKeys(email);
    }

    public void enterSubject(String subject){
        subjectInput.sendKeys(subject);
    }

    public void enterYourMessage(String message){
        yourMessageInput.sendKeys(message);
    }

    public void clickSubmitButton(){
                submitButton.click();
    }

    public void clickOkButtonAlert(){
        driver.switchTo().alert().accept();


    }





    //verificari

    public String getInTouchIsDisplayed() {
        return getInTouchText.getText();
    }

    public String getAlertSuccessMessage(){
        return alertSuccessMessage.getText();

    }


}