package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;


public class ProductsPage {
    WebDriver driver;

    @FindBy(css="#search_product")
    WebElement searchProductInput;

    @FindBy(css=".fa.fa-search")
    WebElement submitSearchInput;


    @FindBy(css=".title.text-center")
    WebElement allProductsText;

    @FindBy(xpath="//h2[@class='title text-center']")
    WebElement searchedProductsList;

    @FindBy(xpath="//div[@class='overlay-content']//p[contains(text(),'Blue Top')]")
    WebElement firstProduct;

    @FindBy(xpath="//div[@class='overlay-content']//p[contains(text(),'Men Tshirt')]")
    WebElement secondProduct;

    @FindBy(css="a[href='/product_details/1']")
    WebElement viewProduct1;

    @FindBy(css="a[data-product-id='1']")
    WebElement addCartFirstProduct;

    @FindBy(css="a[data-product-id='2']")
    WebElement addCartSecondProduct;

    @FindBy(css = "button[data-dismiss='modal']")
    WebElement continueShoppingButton;

    @FindBy(css="a[href='#reviews']")
    WebElement writeReview;
    @FindBy(css="#name")
    WebElement yourName;

    @FindBy(css="#email")
    WebElement email;

    @FindBy(css="#review")
    WebElement addReview;

    @FindBy(css="#button-review")
    WebElement submitReview;

    @FindBy(xpath ="//*[text()='Thank you for your review.']")
    WebElement successMessageReview;

    @FindBy(xpath = "//td[contains(@class, 'cart_description')]//a")
    private List<WebElement> productName;

    @FindBy(xpath="//u[normalize-space()='View Cart']")
    WebElement viewCartButton;





    public static final String URL = "https://automationexercise.com/products";

    public ProductsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }



    public void enterProductName(String productName){
        searchProductInput.sendKeys(productName);


    }

    public void clickSearchButton(){
        submitSearchInput.click();

    }
    public void clickViewProductButton(){
        viewProduct1.click();

    }

    public void enterNameEmailAndReview(String name,String emailAddress,String review){
        yourName.sendKeys(name);
        email.sendKeys(emailAddress);
        addReview.sendKeys(review);
    }

    public void clickSubmitReview(){
        submitReview.click();
    }

    public void hoverOverProductAndClick(){
        Actions action = new Actions(driver);
        action.moveToElement(firstProduct).build().perform();
        addCartFirstProduct.click();
        }

        public void hoverSecondProductAndClick(){
        Actions hover = new Actions(driver);
        hover.moveToElement(secondProduct).build().perform();
        addCartSecondProduct.click();
        }




       public void clickContinueShopping(){
        continueShoppingButton.click();
       }

       public void clickViewCartButton(){
        viewCartButton.click();
       }



    public String verifyWriteYourReviewIsDisplayed(){
        return writeReview.getText();
    }

    public String verifyAllProductsIsDisplayed(){
        return allProductsText.getText();

    }



    public String verifySuccessMessageReviewIsDisplayed(){
        return successMessageReview.getText();

    }






}
