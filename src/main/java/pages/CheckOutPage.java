package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CheckOutPage  {
    WebDriver driver;


    @FindBy(xpath = "(//li[.='myAddress'])[1]")
     WebElement deliveryAddressText;

    @FindBy(xpath = "(//li[.='myAddress'])[2]")
    WebElement billingAddressText;

    @FindBy(tagName = "textarea")
     WebElement commentTextBox;

    @FindBy(xpath = "//tbody//td[@class='cart_product']")
    List<WebElement> reviewCardProducts;

    @FindBy(css=".btn.btn-default.check_out")
    WebElement placeOrderButton;



    public CheckOutPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public void clickPlaceOrderButton(){
        placeOrderButton.click();
    }


}
