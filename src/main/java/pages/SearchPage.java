package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchPage {

    WebDriver driver;
    @FindBy(xpath = "//h2[.='Searched Products']")
    WebElement searchedProductsText;

    @FindBy(xpath = "//div[@class='features_items']/div[@class=\"col-sm-4\"]")
     public List<WebElement> searchedProducts;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
    public String getSearchedProductsText(){
        return searchedProductsText.getText();
    }
}
