package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

public class ViewCartPage {

    WebDriver driver;


    @FindBy(xpath = "//td[contains(@class, 'cart_description')]//a")
     List<WebElement> productName;

    @FindBy(css = "//td[contains(@class, 'cart_price')]/p")
   // tr[id='product-3'] td[class='cart_price'] p
    List<WebElement> price;

    @FindBy(xpath = "//td[contains(@class, 'cart_quantity')]/button")
     List<WebElement> quantity;

    @FindBy(xpath = "//p[contains(@class, 'cart_total_price')]")
     List<WebElement> totalPrice;

    @FindBy(css = "li[class='active']")
    WebElement shoppingCart;

    @FindBy(css = ".btn.btn-default.check_out")
    WebElement proceedToCheckoutButton;

    @FindBy(css = "a[href='/login'] u")
    WebElement registerLoginButton;
    ////u[normalize-space()='Register / Login']

    @FindBy(css = ".fa.fa-times")
     WebElement xButtonDelete1;


    @FindBy(css = "a[data-product-id='2']")
    WebElement xButtonDelete2;

    @FindBy(css = "span[id='empty_cart'] p[class='text-center']")
    WebElement cartEmptyText;

    @FindBy(css = "a[class='cart_quantity_delete']")
     List<WebElement> xButtons;





    public static final String URL = "https://www.automationexercise.com/view_cart";

    public ViewCartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public List<String> getProductsNames() {
        return productName
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }


    public void clickProceedToCheckoutButton(){

        proceedToCheckoutButton.click();
    }


    public void clickXButtonDelete1(){
        xButtonDelete1.click();
    }


    public String getEmptyCartMessage(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(cartEmptyText));
        return cartEmptyText.getText();




    }



     }

