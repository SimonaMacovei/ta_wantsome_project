package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    WebDriver driver;



    @FindBy(css = "input[data-qa='login-email']")
    WebElement emailInput;

    @FindBy(css = "input[placeholder='Password']")
    WebElement passwordInput;

    @FindBy(css = "button[data-qa='login-button']")
    WebElement loginButton;

    @FindBy(css = "li:nth-child(10) a:nth-child(1)")
    WebElement loggedAsUsername;

    @FindBy(css = "a[href='/logout']")
    WebElement logoutButton;

    @FindBy(xpath = "//i[@class='fa fa-trash-o']")
    WebElement deleteAccountButton;

    @FindBy(css = "h2[class='title text-center'] b")
    WebElement accountDeleted;

    @FindBy(xpath = "//p[normalize-space()='Your email or password is incorrect!']")
    WebElement emailAddressAlreadyExist;

    public static final String URL = "https://automationexercise.com/login";

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterEmail(String email) {
        emailInput.sendKeys(email);
    }

    public void enterPassword(String password) {
        passwordInput.sendKeys(password);
    }


    public void clickLoginButton() {
        loginButton.click();
    }

    public void clickLogoutButton() {
        logoutButton.click();
    }


    public void clickDeleteAccountButton(){
        deleteAccountButton.click();
    }



  //verificari
    public String checkLoggedInAsUsernameIsDisplayed() {
        return loggedAsUsername.getText();
    }

    public String getEmailAddressAlreadyExist() {
        return emailAddressAlreadyExist.getText();

    }

    public String checkLogoutButtonIsVisible() {
        return logoutButton.getText();


    }
    public String  checkUserIsNavigatedToLoginPage(){
        return driver.getCurrentUrl();
    }

    public String getAccountDeletedMessage(){
        return accountDeleted.getText();
    }



}
