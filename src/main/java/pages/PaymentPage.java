package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymentPage {
    WebDriver driver;

    @FindBy(css="input[name='name_on_card']")
    WebElement nameOnCard;

    @FindBy(css="input[name='card_number']")
    WebElement cardNumber;

    @FindBy(css="input[placeholder='ex. 311']")
    WebElement ccvInput;

    @FindBy(css="input[placeholder='MM']")
    WebElement monthExpiration;

    @FindBy(css="input[placeholder='YYYY']")
    WebElement yearExpiration;

    @FindBy(css="#submit")
    WebElement payAndConfirmOrder;

    @FindBy(css="div[class='col-sm-9 col-sm-offset-1'] p")
    WebElement confirmationMessageOrder;

   // @FindBy(css=".success_message")
   // WebElement successAlert;


    @FindBy(css=".btn.btn-default.check_out")
    WebElement downloadInvoice;

    @FindBy(css=".btn.btn-primary")
    WebElement continueButton;

    public static final String URL = "https://www.automationexercise.com/payment";


    public PaymentPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

 public void fillNameOnCard(String name){
        nameOnCard.sendKeys(name);
 }
 public void fillCardNumber(int cardNr){
        cardNumber.sendKeys(Integer.toString(cardNr));
 }

 public void fillCCv(int ccvNr){
        ccvInput.sendKeys(Integer.toString(ccvNr));

 }

 public void fillmonthExpiration(int month){
        monthExpiration.sendKeys(Integer.toString(month));
 }

 public void fillYearExpiration(int year){
        yearExpiration.sendKeys(Integer.toString(year));
 }

 public void clickConinuebutton(){
        continueButton.click();
 }

 public void clickPayAndConfirmButton(){
        payAndConfirmOrder.click();
 }




 public String getConfirmationMessageOrder(){
        return confirmationMessageOrder.getText();
 }






}
