package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class SignupPage {
    WebDriver driver;

    @FindBy(css="div[class='signup-form'] h2")
    WebElement newUserSignup;

    @FindBy(css = "input[placeholder='Name']")
    WebElement nameInput;

    @FindBy(css = "input[data-qa='signup-email']")
    WebElement emailAddressInput;


    @FindBy(css = "#password")
    WebElement passwordInput;

    @FindBy(css = "#first_name")
    WebElement firstnameInput;

    @FindBy(css = "#last_name")
    WebElement lastnameInput;

    @FindBy(css = "#address1")
    WebElement addressInput;

    @FindBy(css = "#country")
    WebElement countrySelect;

    @FindBy(css = "#state")
    WebElement stateInput;

    @FindBy(css = "#city")
    WebElement cityInput;

    @FindBy(css = "#zipcode")
    WebElement zipcodeInput;

    @FindBy(css = "#mobile_number")
    WebElement mobileNrInput;

    @FindBy(css = "button[data-qa='signup-button']")
    WebElement signupButton;

    @FindBy(css = "button[data-qa='create-account']")
    WebElement createAccountButton;

    @FindBy(css = "h2[class='title text-center'] b")
    WebElement accountCreated;

    @FindBy(xpath = "//p[normalize-space()='Email Address already exist!']")
    WebElement emailAddressAlreadyExist;






    public static final String URL = "https://automationexercise.com/login";

    public SignupPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterName(String name) {
        nameInput.sendKeys(name);
    }

    public void enterEmail(String email) {
        emailAddressInput.sendKeys(email);
    }

    public void clickONSignupButton() {
        signupButton.click();
    }

    public void enterFirstname(String firstname) {
        firstnameInput.sendKeys(firstname);
    }

    public void enterLastname(String lastname) {
        lastnameInput.sendKeys(lastname);
    }

    public void enterPassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void enterAddress(String address) {
        addressInput.sendKeys(address);
    }

    public void selectCountry(String country) {
        WebElement dropdownElement = driver.findElement(By.cssSelector("#country"));
        dropdownElement.click();
        countrySelect.sendKeys(country);

        Select dropdown = new Select(dropdownElement);

        dropdown.selectByIndex(6);

    }


    public void enterState(String state) {
        stateInput.sendKeys(state);
    }

    public void enterCity(String city) {
        cityInput.sendKeys(city);
    }

    public void enterZipcode(int zipcode) {
        zipcodeInput.sendKeys(Integer.toString(zipcode));
    }
    //sendKeys(Integer.toString())

    public void enterMobileNr(int mobileNr) {
        mobileNrInput.sendKeys(Integer.toString(mobileNr));
    }


    public void clickOnCreateAccountButton() {
        createAccountButton.click();
    }



    //verificari
    public String getAccountIsCreatedWithSuccess() {
        return accountCreated.getText();

    }

    public String checkAnErrorMessageIsDisplayed() {
        return emailAddressAlreadyExist.getText();
    }


    public String newUserSignupIsVisible(){
        return newUserSignup.getText();
    }


   }



