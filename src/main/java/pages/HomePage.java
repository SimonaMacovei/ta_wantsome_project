package pages;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    WebDriver driver;


    @FindBy(css = "a[href='/login']")
     WebElement signupLoginButton;

    @FindBy(css="a[href='/products']")
    WebElement productsButton;
    @FindBy(css = "a[href='/product_details/1']")
    private WebElement viewProduct1Button;

    @FindBy(xpath="//i[@class='fa fa-shopping-cart']")

    WebElement cartButton;

    @FindBy(id = "accordian")
    private WebElement categories;

    @FindBy(css = "a[href='/category_products/1']")
    private WebElement dressCategory;



    @FindBy(xpath = "//*[@id='accordian']/div[1]/div[1]/h4/a/span/i")
    private WebElement womenCategory;

    @FindBy(css="a[href='/test_cases']")
    WebElement testCasesButton;

    @FindBy(css = "div[id='recommended-item-carousel'] a[class='btn btn-default add-to-cart']")
    private WebElement blueTopAddToCartButton;

    @FindBy(css = "div[class='modal-content'] a[href='/view_cart']")
    private WebElement viewCartButton;

    @FindBy(css="a[href='/api_list']")
    WebElement apiTestingButton;

    @FindBy(css="a[href='https://www.youtube.com/c/AutomationExercise']")
    WebElement videoButton;


    @FindBy(css = "a[href='/contact_us']")
    WebElement contactUsButton;

    @FindBy(css="div[class='single-widget'] h2")
    WebElement subscriptionText;

    @FindBy(css="#susbscribe_email")
    WebElement subscribeEmailInput;

    @FindBy(css=".fa.fa-arrow-circle-o-right")
    WebElement scrollUpArrow;

    @FindBy(css="#footer")
    WebElement footer;

    @FindBy(css ="#success-subscribe")
    WebElement alertSuccessSubscribe;

    @FindBy(css=".fa.fa-angle-up")
    WebElement bottomRightArrow;

    @FindBy(css="div[class='item active'] div[class='col-sm-6'] h2")
    WebElement fullFledgedPracticeWebsite;



    public static final String URL = "https://automationexercise.com/";


    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public void scrollDownToTheFooter(){
     JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
     javascriptExecutor.executeScript("arguments[0].scrollIntoView();",footer);

 }

 public void enterEmailForSubscribe(String email){
        subscribeEmailInput.sendKeys(email);
 }

 public void clickArrowToSubscribe(){
        scrollUpArrow.click();

 }
    public void clickProductsButton(){
        productsButton.click();
    }

    public void clickBottomRightArrow(){
        bottomRightArrow.click();
    }

    public void clickCartButton(){

        cartButton.click();
    }

    public void clickTestCasesButton(){
        testCasesButton.click();

    }

    public void clickSignupLoginButton(){
        signupLoginButton.click();
    }

    public void clickApiTestingButton(){
        apiTestingButton.click();
    }

    public void clickVideoTutorialsButton(){
        videoButton.click();
    }





    public String getSubscriptionText(){
        return subscriptionText.getText();
   }



    public String getAlertSuccessMessage(){
        return alertSuccessSubscribe.getText();
    }

    public String verifyUserIsOnHomePage(){
        return driver.getCurrentUrl();

    }
    public String verifyThatPageIsScrolledUpAndFullFledgedPracticeWebsiteForAutomationEngineersTextIsVisibleOnScreen(){
        return fullFledgedPracticeWebsite.getText();
    }




}
