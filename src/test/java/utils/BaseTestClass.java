package utils;

import org.junit.Before;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseTestClass {
    protected WebDriver driver;
    protected static Logger logger = Logger.getLogger(BaseTestClass.class.getName());


    @Before
    public void beforeTest() {


        driver = new ChromeDriver();
        logger.log(Level.INFO,"Am deschis chrome browser");

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));


        driver.navigate().to("https://www.automationexercise.com/products");
        driver.navigate().to("https://www.automationexercise.com/contact_us");
        driver.navigate().back();
        driver.navigate().forward();
        driver.navigate().refresh();

        WebElement consentPopup = driver.findElement(By.xpath("[aria-label='Consent'] [class='fc-button-label']"));
        if (consentPopup.isDisplayed()) {
            consentPopup.click();
        }





    }


}
