package utils;


import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;



public class HelperMethods {
    protected static WebDriver driver;





    public static void handleConsentPopup(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        WebElement consentButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//[aria-label='Consent'] [class='fc-button-label']")));
        consentButton.click();
    }

    public static void hover(WebElement element) {


        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }


    public static WebElement waitForVisibility(By locator, int timeout) {

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }


    public static void waitForElementToBeVisible(WebDriver driver, WebElement locator) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(locator));
    }











}

