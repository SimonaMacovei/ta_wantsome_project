Feature: Signup tests

  @Positive
  Scenario: User registration
    Given Being on Login page
    Then Then Verify 'New User Signup!' is visible
    When Enter credentials name and email

    # name
      | Ana             |
      # email
      | testtest1@yahoo.com |

    When Click Signup button
    When Enter mandatory data with correct data from table


      #password
      | Alabala21  |
      #first name
      | Ana     |
      #last name
      | Popescu    |
      #address
      | Copou      |
      #country
      | Singapore  |
      #state
      | Florida      |
      #city
      | Miami      |
       #zipcode
      | 200457     |
      #mobile nr.
      | 0234567699 |

    When Click Create Account button
    Then Check that "ACCOUNT CREATED!" is visible


  @Positive
  Scenario:Verify that registration doesn't work with existing email address
    Given Being on Login page
    When Enter an email that is already registered
     # name
      | Ana             |
      # email
      | testtest1@yahoo.com |

    When Click Signup button
    Then Check an error message "Email Address already exist!"  is displayed!