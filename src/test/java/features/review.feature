@Regression
Feature: Review product test

@Smoke
Scenario: Add review on product
Given Being on Product page
When Enter product name in search input
And Click search button
When Click on 'View Product' button
Then Verify 'WRITE YOUR REVIEW' is displayed
When Enter name, email and review
      #name
|Anne|
      #email
|test1@test|
      #review
|This is my review|
And Click Submit review button
Then Verify success message 'Thank you for your review.'