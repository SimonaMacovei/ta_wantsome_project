@Regression
Feature: Place order tests


  @Smoke
  Scenario:The user login before and then places the order
    Given Being on home page
    When Click signup/login button
    When Enter correct email and password
    When Click Login button
    Then Verify that 'Logged in as Ana' is visible
    When Hover over first product and click Add to cart
    When Click 'View Cart' button
    Then Verify that cart page is displayed
    When Click Proceed To Checkout button
    And Click Place Order button
    And Enter payment details: Name on Card, Card Number, CVC, Expiration date
      #name on card
      | Ana Popescu |
      #card number
      | 123456789   |
      #Cvc
      | 123         |
      #month expiration
      | 11          |
       #year expiration
      | 2030        |
    When Click Pay and Confirm Order button
    Then Verify confirmation message 'Congratulations! Your order has been confirmed!'
    And Click continue button
    Then Verify user is navigated to home page



