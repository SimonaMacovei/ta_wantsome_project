@Regression
Feature: ContactUs tests


  @Smoke
  Scenario:User submits a message via the contact form
    Given Being on Contact Us Page
    Then Verify 'GET IN TOUCH' is visible
     When Enter name, email, subject and message

    #name
       | Marry                    |
    #email
       | test@test                |
    #subject
       | Here I write the subject |
    #message
       | Here I write the message |

    When Click Submit button
    When Click OK button of the alert
    Then Verify success message 'Success! Your details have been submitted successfully.' is visible

