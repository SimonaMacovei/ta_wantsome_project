@Regression
Feature: Products tests

  @Smoke
  Scenario: Add products in Cart
    Given Being on Product page
    When Hover over first product and click Add to cart
    When Click Continue Shopping button
    When Hover over second product and click 'Add to cart'
    When Click 'View Cart' button
    Then Verify both products are added to Cart
  #  Then Verify their prices, quantity and total price