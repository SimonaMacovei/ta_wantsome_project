@Regression
Feature: Scroll tests


  @Positive
  Scenario: Verify Scroll Down functionality
    Given Being on home page
    Then Verify that home page is visible successfully
    When Scroll down page to the footer
    Then Verify the text 'SUBSCRIPTION' is visible

  @Positive
Scenario: Verify Scroll Up using 'Arrow' button and Scroll Down functionality
   Given Being on home page
   Then Verify that home page is visible successfully
   When Scroll down page to the footer
   Then Verify the text 'SUBSCRIPTION' is visible
   When Click on arrow at bottom right side to move upward
   Then Verify that page is scrolled up and 'Full-Fledged practice website for Automation Engineers' text is visible on screen