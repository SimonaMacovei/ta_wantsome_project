@Regression
Feature: Pages Navigation tests

   @Positive
  Scenario: Verify Test Cases Page
    Given Being on home page
    When Click Test Cases menu button
    Then Verify user is navigated to Test Cases page successfully

  @Positive
  Scenario: Verify API Testing Page
    Given Being on home page
    When Click API Testing menu button
    Then Verify user is navigated to API Testing page successfully

  @Positive
  Scenario: Verify Video Tutorials Page
    Given Being on home page
    When Click Video Tutorials menu button
    Then Verify user is navigated to Video Tutorials page successfully