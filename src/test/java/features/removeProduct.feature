@Regression
Feature: Remove Products test

  @Smoke
  Scenario: Remove Product From Cart
    Given Being on home page
    When Hover over first product and click Add to cart
    And  Click 'View Cart' button
    Then Verify that cart page is displayed
    When Click X button corresponding to product
    Then Verify that product is removed from the cart