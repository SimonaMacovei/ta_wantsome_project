@Regression
Feature: Search Product test

  @Smoke
  Scenario:Searching for a product
    Given Being on Products page
    Then Verify user see 'ALL PRODUCTS' list
    When Enter product name in search input
    And Click search button
    Then Check 'SEARCHED PRODUCTS' is visible
   Then Verify all the products related to search are visible




