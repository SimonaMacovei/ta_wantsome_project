
Feature: Login tests

  @Smoke
  Scenario: Login User with correct email and password
    Given Being on Login page
    When Enter correct email and password
    When Click Login button
    Then Verify that 'Logged in as Ana' is visible

    @Smoke
    Scenario: Login User with incorrect email and password
      Given Being on Login page
      When Enter incorrect email and password
      When Click Login button
      Then Verify error 'Your email or password is incorrect!' is visible

     @Smoke
      Scenario: Logout user
        Given Being on Login page
        When Enter correct email and password
        When Click Login button
        Then Verify that 'Logged in as Ana' is visible
        Then Verify 'Logout' button is visible
        When Click Logout button
        Then Verify that user is navigated to login page



  Scenario: Delete Account after Login
    Given Being on Login page
    When Enter correct email and password
    When Click Login button
    Then Verify that 'Logged in as Ana' is visible
    When Click Delete Account button
    Then Verify that 'ACCOUNT DELETED!' is displayed

