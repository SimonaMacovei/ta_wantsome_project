@Regression
Feature: Subscription Tests
  @Positive
  Scenario:Verify Subscription in home page
    Given Being on home page
    Then Verify that home page is visible successfully
    When Scroll down page to the footer
    Then Verify the text 'SUBSCRIPTION' is visible
    And  Enter email address and click arrow button
    Then Verify success message 'You have been successfully subscribed!' is displayed

  @Positive
  Scenario: Verify Subscription in Cart page
    Given Being on home page
    When Click "Cart" menu button
    And Scroll down page to the footer
    Then Verify the text 'SUBSCRIPTION' is visible
    When Enter email address and click arrow button
    Then  Verify success message 'You have been successfully subscribed!' is displayed

