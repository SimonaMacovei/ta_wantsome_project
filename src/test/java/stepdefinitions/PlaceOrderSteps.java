package stepdefinitions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.CheckOutPage;
import pages.HomePage;
import pages.PaymentPage;
import pages.ViewCartPage;

import java.util.List;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;


public class PlaceOrderSteps {

    HomePage homePage;
    ViewCartPage viewCartPage;

    CheckOutPage checkOutPage;
    PaymentPage paymentPage;

    @When("Click signup\\/login button")
    public void clickSignupLoginButton() {
        logger.log(Level.INFO, "Click Signup/Login button.");
        homePage=new HomePage(driver);
        homePage.clickSignupLoginButton();
    }

    @Then("Verify that cart page is displayed")
    public void verifyThatCartPageIsDisplayed() {
        logger.log(Level.INFO, "Verifying if it's Cart Page");
        viewCartPage=new ViewCartPage(driver);
        Assert.assertEquals("https://www.automationexercise.com/view_cart", ViewCartPage.URL);
    }

    @When("Click Proceed To Checkout button")
    public void clickProceedToCheckoutButton() {
        logger.log(Level.INFO, "Click proceed to checkout button.");
        viewCartPage.clickProceedToCheckoutButton();
        
    }

    @And("Click Place Order button")
    public void clickPlaceOrderButton() {
        logger.log(Level.INFO, "Click place order button");
        checkOutPage=new CheckOutPage(driver);
        checkOutPage.clickPlaceOrderButton();


    }

    @And("Enter payment details: Name on Card, Card Number, CVC, Expiration date")
    public void enterPaymentDetailsNameOnCardCardNumberCVCExpirationDate(List<String> data) {
        logger.log(Level.INFO, "Entering payment details.");
        paymentPage=new PaymentPage(driver);
        paymentPage.fillNameOnCard(data.get(0));
        paymentPage.fillCardNumber(Integer.parseInt(data.get(1)));
        paymentPage.fillCCv(Integer.parseInt(data.get(2)));
        paymentPage.fillmonthExpiration(Integer.parseInt(data.get(3)));
        paymentPage.fillYearExpiration(Integer.parseInt(data.get(4)));





    }

    @When("Click Pay and Confirm Order button")
    public void clickPayAndConfirmOrderButton() {
        logger.log(Level.INFO, "Click pay and confirm order button");
        paymentPage.clickPayAndConfirmButton();
    }


    @Then("Verify confirmation message {string}")
    public void verifyConfirmationMessageCongratulationsYourOrderHasBeenConfirmed(String confirmationMessage) {
        logger.log(Level.INFO, "VVerifying 'Congratulations!Your order has been confirmed' message is displayed");
        Assert.assertEquals(confirmationMessage,paymentPage.getConfirmationMessageOrder());
    }

    @And("Click continue button")
    public void clickContinueButton() {
        logger.log(Level.INFO, "Click Continue Button.");
        paymentPage.clickConinuebutton();
    }

    @Then("Verify user is navigated to home page")
    public void verifyUserNavigatedToHomePage() {
        logger.log(Level.INFO, "Verifying that user navigated to the Home Page.");
        Assert.assertEquals("https://automationexercise.com/",HomePage.URL);

    }
}
