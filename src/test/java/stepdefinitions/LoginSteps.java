package stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.LoginPage;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class LoginSteps {
     LoginPage loginPage = new LoginPage(driver);


     String email = "testtest1@yahoo.com";
     String password = "Alabala21";

     String incorrectEmail ="email@email";
     String incorrectPassword ="Aa1";



    @When("Enter correct email and password")
    public void enterCorrectEmailAndPassword() {
        logger.log(Level.INFO, "Entering correct email and password");
        loginPage = new LoginPage(driver);
        loginPage.enterEmail(email);
        loginPage.enterPassword(password);


    }

    @Then("Verify that {string} is visible")
    public void verifyThatLoggedInAsUsernameIsVisible(String loggedAsUser) {
        logger.log(Level.INFO,"Checking if Logged in as Ana is visible");
        Assert.assertEquals(loggedAsUser,loginPage.checkLoggedInAsUsernameIsDisplayed());

    }

    @When("Enter incorrect email and password")
    public void enterIncorrectEmailAndPassword() {
        logger.log(Level.INFO, "Entering incorrect email and password");
        loginPage = new LoginPage(driver);
        loginPage.enterEmail(incorrectEmail);
        loginPage.enterPassword(incorrectPassword);

    }

    @When("Click Login button")
    public void clickLoginButton() {
        logger.log(Level.INFO, "Click on Login button");
        loginPage.clickLoginButton();
    }

    @Then("Verify error {string} is visible")
    public void verifyErrorYourEmailOrPasswordIsIncorrectIsVisible(String errorMessage) {
        logger.log(Level.INFO,"Verify if error message is visible");
       Assert.assertEquals(errorMessage,loginPage.getEmailAddressAlreadyExist());
    }


    @When("Click Logout button")
    public void clickLogoutButton() {
        logger.log(Level.INFO, "Click on Logout button");
        loginPage.clickLogoutButton();
    }

    @Then("Verify {string} button is visible")
    public void verifyLogoutButtonIsVisible(String logoutB) {
        logger.log(Level.INFO,"Checking if Logout button is visible");
        Assert.assertEquals(logoutB,loginPage.checkLogoutButtonIsVisible());

    }

    @Then("Verify that user is navigated to login page")
    public void verifyThatUserIsNavigatedToLoginPage() {
        logger.log(Level.INFO,"Checking if user is navigated login page");

        String expectedUrl="https://automationexercise.com/login";
        Assert.assertEquals(expectedUrl,loginPage.checkUserIsNavigatedToLoginPage());
    }

    @When("Click Delete Account button")
    public void clickDeleteAccountButton() {

        logger.log(Level.INFO, "Click Delete Account button");
       loginPage.clickDeleteAccountButton();
    }

    @Then("Verify that {string} is displayed")
    public void verifyThatACCOUNTDELETEDIsDisplayed(String accountIsDeleted) {
        logger.log(Level.INFO,"Checking if message Account Deleted is displayed");
        Assert.assertEquals(accountIsDeleted,loginPage.getAccountDeletedMessage());
    }



}
