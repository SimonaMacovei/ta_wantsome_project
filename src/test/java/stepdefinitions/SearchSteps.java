package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.ProductsPage;
import pages.SearchPage;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class SearchSteps {

    String productName="Blue Top";

    ProductsPage productsPage;
    SearchPage searchPage;


    @Given("Being on Products page")
    public void beingOnProductsPage() {
        logger.log(Level.INFO, "Being to Products page");
        driver.get(ProductsPage.URL);


    }

    @Then("Verify user see {string} list")
    public void verifyALLPRODUCTSIsDisplayed(String allProducts) {
        logger.log(Level.INFO, "Verifying if user can see all products list");
        productsPage=new ProductsPage(driver);
        Assert.assertEquals("User is not on All products page",allProducts,productsPage.verifyAllProductsIsDisplayed());
    }

    @When("Enter product name in search input")
    public void enterProductNameInSearchInput() {
        logger.log(Level.INFO, "Entering 'Blue Top' product name in search input");
         searchPage=new SearchPage(driver);
         productsPage=new ProductsPage(driver);
         productsPage.enterProductName(productName);
    }

    @And("Click search button")
    public void clickSearchButton() {
        logger.log(Level.INFO, "Click the search button.");
        productsPage.clickSearchButton();
    }





    @Then("Check {string} is visible")
    public void checkSEARCHEDPRODUCTSIsVisible(String searchedProducts) {
        logger.log(Level.INFO, "Verifying that searched product 'Blue Top' is visible.");
        Assert.assertEquals("Searched products is not visible",searchedProducts,searchPage.getSearchedProductsText());
    }


    @Then("Verify all the products related to search are visible")
    public void verifyAllTheProductsRelatedToSearchAreVisible() {
        logger.log(Level.INFO, "Verifying that all products related to the search are  all displayed in the search list.");
        Assert.assertEquals(1, searchPage.searchedProducts.size());




    }
}
