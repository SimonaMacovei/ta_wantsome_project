package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.HomePage;
import pages.ProductsPage;
import java.util.List;
import java.util.logging.Level;
import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class ProductsSteps {
    HomePage homePage;

    ProductsPage productsPage;



    @Given("Being on home page")
    public void beingOnHomePage() {
        logger.log(Level.INFO, "Being to Home page");
        driver.get(HomePage.URL);
    }

    @When("Click on Products button")
    public void clickOnProductsButton() {
        logger.log(Level.INFO, "Click Product menu button");
        productsPage = new ProductsPage(driver);
        homePage=new HomePage(driver);
       homePage.clickProductsButton();
    }




    @Given("Being on Product page")
    public void beingOnProductPage() {
        logger.log(Level.INFO, "Being to Product page");
        driver.get(ProductsPage.URL);
    }

    @When("Click on {string} button")
    public void clickOnViewProductButton(String viewProduct) {
        logger.log(Level.INFO, "Click on view product button.");
        productsPage=new ProductsPage(driver);
        productsPage.clickViewProductButton();
        
    }

    @Then("Verify {string} is displayed")
    public void verifyWriteYourReviewIsDisplayed(String review) {
        logger.log(Level.INFO, "Verifying that 'Write your review' text is displayed");
        Assert.assertEquals("Review field  is not displayed",review,productsPage.verifyWriteYourReviewIsDisplayed());
    }

    @When("Enter name, email and review")
    public void enterNameEmailAndReview(List<String> info) {
        logger.log(Level.INFO, "Entering name:Anne; email:test1@test and review:This is my review  in the fields");
        productsPage.enterNameEmailAndReview(
                info.get(0),
                info.get(1),
                info.get(2));

    }



    @And("Click Submit review button")
    public void clickSubmitReview() {
        logger.log(Level.INFO, "Click submit review button");
        productsPage.clickSubmitReview();
    }

    @Then("Verify success message {string}")
    public void verifySuccessMessageThankYouForYourReview(String successMessage) {
        logger.log(Level.INFO, "Verifying 'Thank you for your review' message is displayed ");
        Assert.assertEquals("The message is incorrect",successMessage,productsPage.verifySuccessMessageReviewIsDisplayed());
    }
}
