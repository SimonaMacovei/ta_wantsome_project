package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.SignupPage;
import java.util.List;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class SignupSteps {

    SignupPage signupPage;


    @Given("Being on Login page")
    public void beingOnLoginPage() {
        logger.log(Level.INFO, "Being to Login page");
        driver.get(SignupPage.URL);

    }

    @When("Enter credentials name and email")
    public void enterCredentialsNameAndEmail(List<String> data) {
        logger.log(Level.INFO, "Entering name and email address");
        signupPage = new SignupPage(driver);
        signupPage.enterName(data.get(0));
       signupPage.enterEmail(data.get(1));

    }

    @When("Click Signup button")
    public void clickSignupButton() {

        logger.log(Level.INFO, "Click on Signup button");
        signupPage.clickONSignupButton();
    }

    @When("Enter mandatory data with correct data from table")
    public void enterMandatoryDataWithCorrectDataFromTable(List<String> data) {
        logger.log(Level.INFO, "Entering mandatory data");

        signupPage.enterPassword(data.get(0));
        signupPage.enterFirstname(data.get(1));
        signupPage.enterLastname(data.get(2));
        signupPage.enterAddress(data.get(3));
        signupPage.selectCountry(data.get(4));
        signupPage.enterCity(data.get(5));
        signupPage.enterState(data.get(6));
        signupPage.enterZipcode(Integer.parseInt(data.get(7)));
        signupPage.enterMobileNr(Integer.parseInt(data.get(8)));


    }


    @When("Click Create Account button")
    public void clickCreateAccountButton() {
        logger.log(Level.INFO, "Click on Create Account button");
        signupPage.clickOnCreateAccountButton();
    }

    @Then("Check that {string} is visible")
    public void checkThatIsVisible(String accountCreated) {
        logger.log(Level.INFO,"Checking if \"Account created\" is visible");
        Assert.assertEquals(accountCreated, signupPage.getAccountIsCreatedWithSuccess());

    }

    @When("Enter an email that is already registered")

    public void enterAnEmailThatIsAlreadyRegistered(List<String> data) {
        logger.log(Level.INFO, "Entering an email already registered");
        signupPage = new SignupPage(driver);
        signupPage.enterName(data.get(0));
        signupPage.enterEmail(data.get(1));

    }


    @Then("Check an error message {string}  is displayed!")
    public void checkAnErrorMessageIsDisplayed(String errorMessage) {
        logger.log(Level.INFO,"Checking if an error message is displayed");
        Assert.assertEquals(errorMessage,signupPage.checkAnErrorMessageIsDisplayed());

    }

    @Then("Then Verify {string} is visible")
    public void thenVerifyNewUserSignupIsVisible(String newUser) {
        logger.log(Level.INFO,"Verifying that New User Signup is visible on page");
        signupPage=new SignupPage(driver);
        Assert.assertEquals(newUser,signupPage.newUserSignupIsVisible());
    }
}




