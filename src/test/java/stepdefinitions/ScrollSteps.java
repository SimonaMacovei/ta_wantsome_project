package stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.HomePage;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class ScrollSteps {
    HomePage homePage;



    @When("Click on arrow at bottom right side to move upward")
    public void clickOnArrowAtBottomRightSideToMoveUpward() {
        logger.log(Level.INFO, "Click on arrow at bottom right side ");
        homePage = new HomePage(driver);
        homePage.clickBottomRightArrow();

    }

    @Then("Verify that page is scrolled up and {string} text is visible on screen")
    public void verifyThatPageIsScrolledUpAndFullFledgedPracticeWebsiteForAutomationEngineersTextIsVisibleOnScreen( String expectedText) {
        logger.log(Level.INFO, "Verifying that page is scrolled up and 'Full-Fledged practice website for Automation Engineers' text is visible ");
        Assert.assertEquals(expectedText,homePage.verifyThatPageIsScrolledUpAndFullFledgedPracticeWebsiteForAutomationEngineersTextIsVisibleOnScreen());
    }
}
