package stepdefinitions;


import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.HomePage;


import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class PagesSteps {
    
    HomePage homePage;




    @When("Click Test Cases menu button")
    public void clickTestCasesMenuButton() {
        logger.log(Level.INFO, "Click Test Cases menu button");
        homePage=new HomePage(driver);
        homePage.clickTestCasesButton();
    }

    @Then("Verify user is navigated to Test Cases page successfully")
    public void verifyUserIsNavigatedToTestCasesPageSuccessfully() {
        logger.log(Level.INFO,"Checking if user is navigated to Test Cases Page");

        String expectedUrl = "https://www.automationexercise.com/test_cases";
        Assert.assertEquals( expectedUrl,"https://www.automationexercise.com/test_cases");
    }

    @When("Click API Testing menu button")
    public void clickAPITestingMenuButton() {
        logger.log(Level.INFO, "Click API Testing menu button");
        homePage=new HomePage(driver);
        homePage.clickApiTestingButton();
    }

    @Then("Verify user is navigated to API Testing page successfully")
    public void verifyUserIsNavigatedToAPITestingPageSuccessfully() {
        logger.log(Level.INFO,"Checking if user is navigated to API Testing Page");

        String expectedUrl = "https://www.automationexercise.com/api_list";
        Assert.assertEquals(expectedUrl,"https://www.automationexercise.com/api_list");

    }

    @When("Click Video Tutorials menu button")
    public void clickVideoTutorialsMenuButton() {
        logger.log(Level.INFO, "Click Video Tutorials menu button");
        homePage=new HomePage(driver);
        homePage.clickVideoTutorialsButton();
    }

    @Then("Verify user is navigated to Video Tutorials page successfully")
    public void verifyUserIsNavigatedToVideoTutorialsPageSuccessfully() {
        logger.log(Level.INFO,"Checking if user is navigated to Video Tutorials Page");
        String expectedUrl = "https://www.youtube.com/c/AutomationExercise";
        Assert.assertEquals(expectedUrl,"https://www.youtube.com/c/AutomationExercise");

    }
}
