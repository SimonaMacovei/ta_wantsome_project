package stepdefinitions;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.ViewCartPage;
import java.util.logging.Level;
import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;


public class RemoveProductSteps {

    ViewCartPage viewCartPage;


    @When("Click X button corresponding to product")
    public void clickXButtonCorrespondingToProduct() {
        logger.log(Level.INFO, "Click on the x button corresponding to product to delete");
        viewCartPage= new ViewCartPage(driver);
        viewCartPage.clickXButtonDelete1();
    }

    @Then("Verify that product is removed from the cart")
    public void verifyThatProductIsRemovedFromTheCart() {
        logger.log(Level.INFO, "Verifying that product is removed from the cart");
        String expectedEmptyCart="Cart is empty! Click here to buy products.";
        Assert.assertEquals(expectedEmptyCart,viewCartPage.getEmptyCartMessage());
    }
}
