package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.ContactUsPage;


import java.util.List;
import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class ContactUsSteps  {

    ContactUsPage contactUsPage;




    @Given("Being on Contact Us Page")
    public void beingOnContactUsPage() {
        logger.log(Level.INFO, "Being to ContactUs page");
        driver.get(ContactUsPage.URL);
    }


    @Then("Verify {string} is visible")
    public void verifyGETINTOUCHIsVisible(String text) {
        logger.log(Level.INFO,"Verifying Get in touch text is visible");
        contactUsPage = new ContactUsPage(driver);
        Assert.assertEquals(text,contactUsPage.getInTouchIsDisplayed());
    }

    @When("Enter name, email, subject and message")
    public void enterNameEmailSubjectAndMessage(List<String> data) {
        logger.log(Level.INFO, "Entering name:Marry,email:test@test,subject:Here I write the subject,mesage:Here I write the message.");
        contactUsPage.enterName(data.get(0));
        contactUsPage.enterEmail(data.get(1));
        contactUsPage.enterSubject(data.get(2));
        contactUsPage.enterYourMessage(data.get(3));

    }

    @When("Click Submit button")
    public void clickSubmitButton() {
        logger.log(Level.INFO, "Click Submit Button");
        contactUsPage.clickSubmitButton();
    }

    @When("Click OK button of the alert")
    public void clickOKButtonOfTheAlert() {
        logger.log(Level.INFO, "Click Ok Alert Button");
        contactUsPage.clickOkButtonAlert();
    }

    @Then("Verify success message {string} is visible")
    public void verifySuccessMessageSuccessYourDetailsHaveBeenSubmittedSuccessfullyIsVisible(String successMessage) {
        logger.log(Level.INFO,"Verify message:'Success! Your details have been submitted successfully.' is  visible");
        Assert.assertEquals(successMessage,contactUsPage.getAlertSuccessMessage());
    }
}
