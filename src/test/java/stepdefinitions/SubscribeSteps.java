package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.HomePage;
import pages.ViewCartPage;
import utils.HelperMethods;

import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class SubscribeSteps {

    HomePage homePage;
    ViewCartPage viewCartPage;
    HelperMethods helperMethods;

    String email="test@test";




    @When("Scroll down page to the footer")
    public void scrollDownToTheFooter() {
        logger.log(Level.INFO,"Scrolling down to the footer");
        homePage.scrollDownToTheFooter();
    }

    @Then("Verify the text {string} is visible")
    public void verifyTheTextSubscription(String textSubscription) {
        logger.log(Level.INFO,"Verifying subscription text is visible");
        Assert.assertEquals(textSubscription,homePage.getSubscriptionText());
    }

    @And("Enter email address and click arrow button")
    public void enterEmailAddressAndClickArrowButton() {
        logger.log(Level.INFO, "Entering email address and click arrow button");
        homePage.enterEmailForSubscribe(email);
        homePage.clickArrowToSubscribe();
    }

    @Then("Verify success message {string} is displayed")
    public void checkSuccessMessageYouHaveBeenSuccessfullySubscribed(String expectedMessage) {
        logger.log(Level.INFO,"Checking if message \"You have been successfully subscribed!\" is displayed");
        Assert.assertEquals("You have not been successfully subscribed!",expectedMessage,homePage.getAlertSuccessMessage());

    }

    @Then("Verify that home page is visible successfully")
    public void verifyThatHomePageIsVisibleSuccessfully() {
        logger.log(Level.INFO,"Checking if user is on Home Page");
        homePage = new HomePage(driver);
        String expectedUrl="https://automationexercise.com/";
        Assert.assertEquals(expectedUrl,homePage.verifyUserIsOnHomePage());
    }

    @When("Click {string} menu button")
    public void clickMenuButton(String button) {
        logger.log(Level.INFO, "Click Cart menu button");
        homePage=new HomePage(driver);
        homePage.clickCartButton();

    }
}
