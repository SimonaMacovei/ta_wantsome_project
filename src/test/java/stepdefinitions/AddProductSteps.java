package stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.ViewCartPage;
import pages.ProductsPage;


import java.util.logging.Level;

import static stepdefinitions.Hooks.driver;
import static stepdefinitions.Hooks.logger;

public class AddProductSteps {
    ProductsPage productsPage;
    ViewCartPage viewcartPage;






    @When("Hover over first product and click Add to cart")
    public void hoverOverFirstProductAndClickAddToCart() {
        logger.log(Level.INFO, "Hover over first product Blue Top and click add to cart");
        productsPage = new ProductsPage(driver);
        viewcartPage= new ViewCartPage(driver);

        productsPage.hoverOverProductAndClick();


    }

    @When("Click Continue Shopping button")
    public void clickContinueShoppingButton() {
        logger.log(Level.INFO, "Click continue shopping button");
        productsPage.clickContinueShopping();
    }

    @When("Hover over second product and click {string}")
    public void hoverOverSecondProductAndClickAddToCart(String addToCart) {
        logger.log(Level.INFO, "Hover over second product  Men Tshirt and click add to cart");

        productsPage.hoverSecondProductAndClick();
    }

    @When("Click {string} button")
    public void clickViewCartButton(String viewCart) {
        logger.log(Level.INFO, "Click View Cart Button");
        productsPage.clickViewCartButton();
    }

    @Then("Verify both products are added to Cart")
    public void verifyBothProductsAreAddedToCart() {
        logger.log(Level.INFO, "Verifying both products are added to cart");
        Assert.assertEquals(2,viewcartPage.getProductsNames().size());


    }
}
